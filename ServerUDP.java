
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;



public class ServerUDP extends Thread{
    private static int port_server = 8080; 
    private DatagramSocket socket_server; 
    private ThreadPoolExecutor pool_thread; 
    private int pool_size;  

    public ServerUDP(){
        try {
            this.socket_server = new DatagramSocket(port_server);
            System.out.println("Server listenning on port: " + port_server);
            
        } catch (IOException e) {
            System.err.println(e);
        }

        this.pool_size = 10; 
        this.pool_thread = (ThreadPoolExecutor) Executors.newFixedThreadPool(this.pool_size);
    }


    @Override
    public void run() {
        while (true) {
            try {
                DatagramPacket packet_client = new DatagramPacket(new byte[1000], 1000);
                this.socket_server.receive(packet_client);

                System.out.println("nb process used : " + this.pool_thread.getActiveCount());

                if(this.pool_thread.getActiveCount() == this.pool_thread.getMaximumPoolSize()){
                    byte[] data_server = "sorry ! every processes are being used".getBytes(); 
                    this.socket_server.send(new DatagramPacket(data_server, data_server.length, packet_client.getAddress(), packet_client.getPort()));
                }
                else if(new String(packet_client.getData()).contains("connection request")){

                    this.pool_thread.submit(new Process(packet_client));
                }
                else{
                    byte[] data_server = "please ask valid request".getBytes(); 
                    this.socket_server.send(new DatagramPacket(data_server, data_server.length, packet_client.getAddress(), packet_client.getPort()));
                }
            
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        ServerUDP server = new ServerUDP(); 
        server.start();
    }
}
