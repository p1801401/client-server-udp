import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

public class ClientUDP extends Thread {

    private int port_server; 
    private InetAddress address_server;
    private Scanner sc; 
    private DatagramSocket socket_client;

    public ClientUDP(InetAddress address_server, int port_server){
        try { 
            this.address_server = address_server;
            this.port_server = port_server; 
            
            this.socket_client = new DatagramSocket();

        } catch (SocketException e) {
            e.printStackTrace();
        }

        this.sc = new Scanner(System.in); 
    }


    @Override
    public void run() {
        if(this.socket_client.isClosed()){
            try {
                this.socket_client = new DatagramSocket();
            } catch (SocketException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        while (true) {
            try {                
                String msg = sc.nextLine(); 
                byte[] data_client = new byte[msg.length()]; 
                data_client = msg.getBytes();  
                DatagramPacket packet_client = new DatagramPacket(data_client, data_client.length, address_server, port_server); 
                socket_client.send(packet_client);

                DatagramPacket packet_server = new DatagramPacket(new byte[1000], 1000); 
                socket_client.receive(packet_server);
                System.out.println("client recieved : " + new String(packet_server.getData()) + " by " + packet_server.getAddress() + ":" + packet_server.getPort());

                if(new String(packet_server.getData()).contains("allocated process")){
                    this.address_server = packet_server.getAddress();
                    this.port_server = packet_server.getPort();  
                }

                if(new String(packet_server.getData()).contains("close connection")){
                    break;
                }

            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } 
        }

        try {
            this.join(1);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            ClientUDP c = new ClientUDP(InetAddress.getByName("localhost"), 8080);
            c.start();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } 
        
    }

}
