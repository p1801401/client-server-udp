
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class Process extends Thread {
    private DatagramSocket socket_process; 
    private DatagramPacket packet_client; 
    private DatagramPacket packet_server; 

    public Process(DatagramPacket packet_client){

        this.packet_client = packet_client; 
        
        System.out.println("Message recieved : " + new String(packet_client.getData()));
        System.out.println("Author : " + packet_client.getAddress() + ":" + packet_client.getPort());
        
        try {                
                
            this.socket_process = new DatagramSocket(); 
            
            byte[] data_process = "Message recieved ! this is your allocated process".getBytes();
            this.packet_server = new DatagramPacket(data_process , data_process.length, this.packet_client.getAddress(), this.packet_client.getPort());
            this.socket_process.send(this.packet_server);
            
        } catch (IOException e) {
            System.err.println(e);
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while(true){        
            try {
                packet_client = new DatagramPacket(new byte[1000], 1000);
                this.socket_process.receive(packet_client);
                System.out.println("Message recieved : " + new String(packet_client.getData()));
                System.out.println("Author : " + packet_client.getAddress() + ":" + packet_client.getPort());

                if(new String(packet_client.getData()).contains("close connection")){
                    byte[] data_process = "Message recieved ! close connection".getBytes();
                    this.packet_server = new DatagramPacket(data_process , data_process.length, this.packet_client.getAddress(), this.packet_client.getPort());
                    this.socket_process.send(this.packet_server);

                    break;
                }
                else{
                
                    byte[] data_process = "Message recieved ! ".getBytes();
                    this.packet_server = new DatagramPacket(data_process , data_process.length, this.packet_client.getAddress(), this.packet_client.getPort());
                    this.socket_process.send(this.packet_server);
                    
                }
            } catch (IOException e) {
                System.err.println(e);
                e.printStackTrace();
            }
        }

        try {
            
            this.join(1);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
}
